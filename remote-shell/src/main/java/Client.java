/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java;

import java.net.*;

/**
 *
 * @author darcdev
 */
public class Client {
        
     public static void main(String args[]) { 
        Socket socket;
        InetAddress IP;
        int Puerto = 2050;
        
        try {    
            System.out.println("Estableciendo Conexion el servidor");
            IP = InetAddress.getByName("192.168.0.25");
            socket = new Socket(IP, Puerto);
            System.out.println("Conexion establecida con el servidor");
            
             Login log = new Login(socket);
             log.setVisible(true);
         }
         catch(Exception e){
    
           System.out.println(e);
    
          }   
     }
}
