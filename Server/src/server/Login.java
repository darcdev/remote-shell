/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 *
 * @authors Cristian Vera , Sebastian Baez , Diego Cabrera
 */
public class Login {
    
    public Login(){};
    
    public boolean checkUserData (String username , String password){
    
        File userdb = new File("users.db");
        String usernameFile ;
        String passwordFile ;
        FileReader readerFile;
        BufferedReader bufferFile;
        
        boolean checkUser = false;
        try {
            readerFile = new FileReader(userdb);
            bufferFile = new BufferedReader(readerFile);
            String contenido;
            while ((contenido = bufferFile.readLine()) != null) {
                String[] param = contenido.split(":");
		usernameFile = param[0];
                passwordFile = param[1];
                
                if(usernameFile.equals(username)){
                    
                    Cifrador cfd = new Cifrador();
                    password = cfd.hash(password);
                    if(passwordFile.equals(password)){
                         checkUser = true;
                         break;
                    }
                    else {
                        checkUser = false;
                        break;
                    }
                }
                else{ 
                    checkUser = false;
                }
	    }
	    bufferFile.close();
	}
	catch(Exception e) {
		System.out.println(e);	
        }
        return checkUser;
    }  
    
}
