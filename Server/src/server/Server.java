/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.net.*;
import java.io.*;
// "user:4swww;user:ssuss;mkdir taller;cpu:cositarica:memoria" 
/**
 *
 * @authors Cristian Vera , Sebastian Baez , Diego Cabrera
 */
public class Server {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
                
         ServerSocket sServer;
         Socket sConexion = null;
         try {
            System.out.println("Creando un socket en el puerto 2050");
            sServer =  new ServerSocket(2050);
	    System.out.println("Esperando conexiones...");
	    sConexion = sServer.accept();
	    System.out.println("Se conecto un cliente...");
	}
	catch(Exception e) {
	    System.out.println(e);
        } 
         
        Controller cont = new Controller();
        DataRetrieve dtrThread = new DataRetrieve(sConexion , cont);
        dtrThread.start();
        DataSend dtsThread = new DataSend(sConexion , cont);
        dtsThread.start();
    }
}


class Controller  {
    
    String message = "";
    
    public Controller(){
    }
    
    public String getMessage (){
        return this.message;
    }
    
    public void setMessage (String message){
        this.message = message;
    }
}

class DataRetrieve extends Thread {
    
    Socket ss;
    Controller controller;
    
    public DataRetrieve(Socket socket , Controller cont) { 
        this.ss = socket;  
        this.controller = cont;
    }
 
    public void run(){
      
       try {
           DataInputStream input;
           String message = "";
           
           do {
               input = new DataInputStream(this.ss.getInputStream());
               message = input.readUTF();
               controller.setMessage(message);
           }

           while(message.equals(""));
       }
       catch(Exception e){
           System.out.println(e);
       }        
    
    }   
}


class DataSend extends Thread {
    
    Socket ss;
    Controller controller;
    
    public DataSend(Socket socket , Controller cont) { 
        this.ss = socket;  
        this.controller = cont;
    }
 
    @Override
    public void run(){
      
       try {

           DataOutputStream output;
           String message;
           
           message = "";
           String messageController;
           do {
               messageController = controller.getMessage();
               String[] separateMessage = messageController.split(";");
     
              // System.out.println(separateMessage[0]);
              // System.out.println(separateMessage[1]);
              // System.out.println(separateMessage[2]);
              /* if(!mensaje[0].equals(":")){
                   respuesta = loguear();
               }
               if(mensaje[1]){
                   respuesta = registrar();
               }
               if(mensaje[2]){
                   respuesta = comandos();
               }
               */
               //responseGraficas = graficas()
                
               //String response = r_login + ";" + r_register + ";" + r_commands;
                              
               output = new DataOutputStream(this.ss.getOutputStream());
               output.writeUTF("holas");

           }

           while(message.equals(""));
       }
       catch(Exception e){
           System.out.println(e);
       }        
    
    }   
    
    
}


